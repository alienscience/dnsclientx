#!/bin/sh

set -e

cargo test --features sync
cargo test --lib --features smol-async
cargo test --lib --features std-async
cargo test --lib --features tokio-async
