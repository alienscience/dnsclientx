#!/bin/sh

set -e

cargo clippy --features sync -- --deny warnings
cargo clippy --features smol-async -- --deny warnings
cargo clippy --features std-async -- --deny warnings
cargo clippy --features tokio-async -- --deny warnings
