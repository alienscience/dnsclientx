#!/bin/sh

[ $# -eq 1 ] || { cat << EOUSE ; exit 1 ; }
  Usage: $0 [major | minor | patch]

Release dnsclientx.
EOUSE

cargo release --features 'sync' --no-dev-version $1
