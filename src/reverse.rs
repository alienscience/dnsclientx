use std::net::IpAddr;

pub fn reverse_dns_query(ip: IpAddr) -> Vec<u8> {
    let prefix = reverse_ip(&ip);
    match ip {
        IpAddr::V4(_) => format!("{prefix}.in-addr.arpa").into_bytes(),
        IpAddr::V6(_) => format!("{prefix}.ip6.arpa").into_bytes(),
    }
}

/// Reverse an IP address.
/// # Example
/// ```
/// # use std::net::IpAddr;
/// # use std::str::FromStr;
/// use dnsclientx::reverse_ip;
///
/// let ipv4 = IpAddr::from_str("127.0.0.1").unwrap();
/// let ipv6 = IpAddr::from_str("::1").unwrap();
/// let rev_ipv4 = reverse_ip(&ipv4);
/// let rev_ipv6 = reverse_ip(&ipv6);
///
/// assert!(rev_ipv4 == "1.0.0.127");
/// assert!(rev_ipv6 == "1.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0.0");
/// ```
pub fn reverse_ip(ip: &IpAddr) -> String {
    match ip {
        IpAddr::V4(i4) => {
            let octets = i4.octets();
            format!("{}.{}.{}.{}", octets[3], octets[2], octets[1], octets[0])
        }
        IpAddr::V6(i6) => {
            let nibbles: Vec<_> = i6
                .octets()
                .iter()
                .flat_map(|b| byte_to_nibbles(*b))
                .rev()
                .map(|n| n.to_string())
                .collect();
            nibbles.join(".")
        }
    }
}

fn byte_to_nibbles(b: u8) -> Vec<u8> {
    let hn = (b & 0xf0) >> 4;
    let ln = b & 0x0f;
    vec![hn, ln]
}

