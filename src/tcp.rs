pub fn tcp_query(packet: &[u8]) -> Vec<u8> {
    let query_len = packet.len();
    let mut ret = Vec::with_capacity(2 + query_len);
    ret.push((query_len >> 8) as u8);
    ret.push(query_len as u8);
    ret.extend_from_slice(packet);
    ret
}
