use std::io::{Error, ErrorKind};

pub fn as_io_error<E>(kind: ErrorKind) -> impl Fn(E) -> Error
where
    E: Into<Box<dyn std::error::Error + Send + Sync>>,
{
    move |e: E| Error::new(kind, e)
}
