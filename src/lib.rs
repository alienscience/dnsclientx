//! # A Small and Flexible DNS Client
//! The DNS Client has the following features:
//! * Resolves IPv4 and IPv6 addresses.
//! * Supports reverse lookups of IPv4 and IPv6 addresses.
//! * Can lookup a nameserver for a domain.
//!
//! There are multiple features available and one feature must be chosen:
//!
//! | Feature     |  Description |
//! |-------------|-----------------|
//! | sync        | Synchronous API |
//! | std-async   | Async API using async-std |
//! | smol-async  | Async API using smol |
//! | tokio-async | Async API using tokio |

#![forbid(unsafe_code)]
#![forbid(missing_docs)]

mod err;
mod resolve;
mod reverse;
#[cfg(feature = "smol-async")]
mod smol_async;
#[cfg(feature = "std-async")]
mod std_async;
#[cfg(feature = "sync")]
mod sync;
mod tcp;
#[cfg(feature = "tokio-async")]
mod tokio_async;

pub use resolve::DNSClient;
pub use reverse::reverse_ip;
