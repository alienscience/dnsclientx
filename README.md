# IMPORTANT

This repository has moved to: [https://code.alienscience.org/alienscience/dnsclientx](https://code.alienscience.org/alienscience/dnsclientx).

# A simple and secure DNS client

[API documentation](https://docs.rs/dnsclientx)

This is a fork of the `dnsclient` crate which adds extra features.

This crate can resolve IPv4 and IPv6 addresses and supports International Domain Names (IDNA). This DNS client also supports reverse DNS lookups of IP to name and it can lookup a nameserver for a domain.

The client transparently falls back to TCP when a truncated response is received.

The API is simple and the crate supports sync, async-std, tokio and smol.

## Development

The documentation is generated for the `sync` feature. To test all features use `scripts/test.sh`.
